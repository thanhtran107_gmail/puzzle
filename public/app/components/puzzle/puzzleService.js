angular.module('puzzleApp').factory('puzzleService', function(localStorageService) {
	var self = {};

	self.isCorrect = function(cell, pass){
		if(cell.join(',')===pass.join(',')){
			return true;
		}else{
			return false;
		}
	};

	self.convertUX = function(data){
		var result = [];
		// iMax is faster
		var iMax = data.length;
		for(var i=0;i<iMax;i++){
			result[i]={
				'class': data[i]
			}
		}
		return result;
	};
	self.saveGame = function(cell, move){
		// save to local storage
		localStorageService.set('cell',cell);
		localStorageService.set('move',move);
	};
	self.getTimeTotal = function(cell, move){
		// save to local storage
		localStorageService.set('cell',cell);
		localStorageService.set('move',move);
	};
	self.newTimeTotal = function(){
		var d = new Date();
		var n = d.getTime();
		// set new timer
		localStorageService.set('time', {
			last: n,
			used: 0
		});
	};
	self.addTimeTotal = function(){
		var d = new Date();
		var n = d.getTime();
		var timer=localStorageService.get('time');

		if(!timer||timer.length<1){
			// set new timer
			self.newTimeTotal();
		}else{
			// increment current timer
			localStorageService.set('time', {
				last: n,
				used: parseInt(n-timer.last)+parseInt(timer.used)
			});
		}
	};

	return self;
});