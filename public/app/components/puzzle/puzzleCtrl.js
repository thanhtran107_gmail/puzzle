angular.module('puzzleApp').controller('PuzzleCtrl', function ($filter, localStorageService, puzzleService, $window) {
	var pass = ['r1-c1','r1-c2','r1-c3','r2-c1','r2-c2','r2-c3','r3-c1','r3-c2','r3-c3'];

	this.eDrop = function(event, ui, data){
		// ignore if drop on self
		if(this.dragIndex===data){
			return false;
		}
		// count move
		this.move++;

		// process UI event
		var tmp=this.cell[data];
		this.cell[data]=this.cell[this.dragIndex];
		this.cell[this.dragIndex]=tmp;

		// check status
		if(puzzleService.isCorrect(this.cell,pass)){
			this.state = 'complete';
			puzzleService.addTimeTotal();
		}
			this.timer=localStorageService.get('time');
		// save to local storage
		puzzleService.saveGame(this.cell,this.move);
	}

	this.eDrag = function(event, ui, data){
		this.dragIndex=data;
	}

	this.reset = function(){
		this.state = 'playing'; // complete | playing
		this.move = 0;
		this.dragIndex = 0;
	}

	this.eRestart = function(){
		this.reset();
		this.cell = _.shuffle(this.cell);
		this.cellUX = puzzleService.convertUX(this.cell);
		// save to local storage
		puzzleService.saveGame(this.cell,this.move);
		puzzleService.newTimeTotal();
	}

	this.eInit = function(){
		this.reset();
		var sCell = localStorageService.get('cell');
		// is in local storage
		if(!sCell||sCell.length<1){
			this.cell = _.shuffle(pass);
			this.cellUX = puzzleService.convertUX(this.cell);
			puzzleService.newTimeTotal();
		}else{
			this.cell = angular.copy(sCell);
			this.cellUX = puzzleService.convertUX(this.cell);
			this.move = localStorageService.get('move');
		}
	}

});