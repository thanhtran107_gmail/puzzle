var express = require('express'),
	path = require('path');

var app = express();
app.use(express.static('public'))

app.set("view engine","vash")
app.set('views', path.join( __dirname, '../../vash/view') );
app.get('/', function (req, res) {
    res.render('index', {title: 'Puzzle Test'});
});

var listener = app.listen(3000, function () {
  console.log('Server listening on port '+listener.address().port);
})