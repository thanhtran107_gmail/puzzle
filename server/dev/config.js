var express = require('express'),
	path = require('path');

var app = express();
app.use(express.static('public'))

app.set("view engine","vash")
app.set('views', path.join( __dirname, '../../vash/view') );
app.get('/', function (req, res) {
    res.render('index', {title: 'Puzzle Test'});
});

module.exports = app;