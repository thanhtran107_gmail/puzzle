module.exports = function(grunt) {

  grunt.initConfig({
    // config webserver
    express: {
      dev: {
        options: {
          port: 5000,
          hostname: 'localhost',
          server: 'server/dev/config.js'
        }
      }
    },
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'public/asset/css/main.css': 'sass/main.scss'
        }
      }
    },
    watch: {
      sass: {
        files: ['sass/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false,
        },
      },
      karma: {
        files: ['test/**/*.js'],
        tasks: ['karma']
      }
    },
    copy: {
      // copy frontend dependencies
      main: {
        files: [
        {expand: true, cwd: 'bower_components/jquery/dist/', src: ['jquery.min.js'], dest: 'public/asset/plugin/jquery/', filter: 'isFile'},
        {expand: true, cwd: 'bower_components/jquery-ui/', src: ['jquery-ui.min.js'], dest: 'public/asset/plugin/jquery-ui/', filter: 'isFile'},
        {expand: true, cwd: 'bower_components/bootstrap/dist/', src: ['**'], dest: 'public/asset/plugin/bootstrap/'},
        {expand: true, cwd: 'bower_components/lodash/dist/', src: ['lodash.min.js'], dest: 'public/asset/plugin/lodash/', filter: 'isFile'},
        {expand: true, cwd: 'bower_components/', src: ['angular/angular.min.js'], dest: 'public/asset/plugin/', filter: 'isFile'},
        {expand: true, cwd: 'bower_components/angular-local-storage/dist/', src: ['angular-local-storage.min.js'], dest: 'public/asset/plugin/angular-local-storage', filter: 'isFile'},
        {expand: true, cwd: 'bower_components/angular-dragdrop/src/', src: ['angular-dragdrop.min.js'], dest: 'public/asset/plugin/angular-dragdrop', filter: 'isFile'}
        ]
      }
    },
    karma: {  
      unit: {
        options: {
          frameworks: ['jasmine'],
          singleRun: true,
          browsers: ['Chrome'],
          configFile: 'karma.conf.js'
        }
      }
    }
  });


  // grunt dependencies
  
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-karma');

  // grunt task dev
  grunt.registerTask('dev', ['express', 'copy', 'watch', 'express-keepalive']);
  grunt.registerTask('test', ['express', 'copy', 'karma', 'watch', 'express-keepalive']);
  // copy prod files to public folder
  grunt.registerTask('prod', ['copy','sass']);

};

