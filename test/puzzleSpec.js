describe('puzzleApp', function() {
  beforeEach(module('puzzleApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('PuzzleCtrl', function() {

    it('reset game value with reset()', function() {
      var controller = $controller('PuzzleCtrl');

      // setup
      controller.move=1;
      controller.reset();

      // confirm
      expect(controller.move).toEqual(0);
    });

    it('make sure eDrop() swap items 2 with item 1', function() {
      var controller = $controller('PuzzleCtrl');

      // setup
      controller.dragIndex = 0;
      controller.cell = ['1','2'];
      expect(controller.cell[0]).toEqual('1');
      expect(controller.cell[1]).toEqual('2');
      controller.eDrop(1,1,1);

      // confirm
      expect(controller.cell[0]).toEqual('2');
      expect(controller.cell[1]).toEqual('1');
    });

    it('make sure eRestart() rnd new cells', function() {
      var controller = $controller('PuzzleCtrl');

      // setup
      var compare=[];
      controller.cell = [];
      for(var i=0;i<9;i++){
        controller.cell.push('i'+i);
        compare.push('i'+i);
      }

      // loop a few times to reduce chance of rnd match
      for(var i=0;i<9;i++){
        if(controller.cell.join(',')===compare.join(',')){
          controller.eRestart();
        }else{
          break;
        }
      }

      // confirm
      expect(controller.cell.join(',')).not.toEqual(compare.join(','));
    });
  });
});